package cz.cvut.fel.ts;

import cz.cvut.fel.ts.Factorial;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Factorial_test {
    @Test
    public void factorialTest(){
        Factorial myFactClass = new Factorial();
        long expectedResult = 120;
        long result  = myFactClass.factorial(5);

        try{
            assertEquals(expectedResult, result);
        }catch (Exception e){
            System.out.println("Zachycena vyjímka");
        }
    }
}
